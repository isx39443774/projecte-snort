# Creació de regles

Les regles de Snort, són un o diversos fitxers. Els quals contindran els patrons que ha de cercar i les accions a pendre segons el tipus de paquet.

Els fitxers han de ser creats amb aquest nom **"nom_qualsevol.rules"** i els guardarem a ```/etc/snort/rules```, aquestes regles poden realitzar diverses accions:   

  1.  **alert** -> generara una alerta que es printara per pantalla i posteriorment es desara el paquet
  2. **log** -> desar el paquet
  3. **pass** -> ignorar el paquet
  4. **drop** -> bloquejar el paquet i desar-lo
  5. **reject** -> bloquejar el paquet, desar-lo i enviar una resposta.
  6. **sdrop** -> bloquejar el paquet i no desar-lo

Per cada fitxer creat haurem de fer un **include** dins del fitxer de configuració (**/etc/snort/snort.conf**)

## Format d'una regla

El format que segueixen totes les regles és el següent:

```
accio protocol IP_origen Port_origen -> IP_destí Port_destí (msg:"missatge";sid:1000001;classtype:xxxxx-xxxx;rev:1;)

msg -> és el missatge descriptiu del que fa la regla
sid -> identificador de la regla
rev -> versió de la regla
classtype -> ens permet classificar les regles
```

### Exemple de regla:
>```
>alert icmp any any -> $HOME_NET any (msg:"Algú ens esta fent ping"; sid:1000001; classtype: icmp-event;rev:1;)
>```
> Aquesta regla creara una alerta que informara de que algú ens esta fent ping.
>
> alert -> es l'acció a realitzar
>
> icmp -> el protocol
>
> any -> IP origen
>
> any -> Port origen
>
> $HOME_NET -> ip a "protegir", definida previament en el fitxer de configuració
>
> any -> Port destí
>
> msg -> missatge
>
> sid -> ID regla Snort, els nombres inferiors a 1.000.000 estan reservats.
>
> classtype -> categoria que se li assigna en aquesta regla
>
> rev -> ens informa de la versió de la regla
