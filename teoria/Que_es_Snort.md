# Què és Snort?

Snort és un IDS (Sistema de detecció d'intrusos), que també pot
funcionar com un IPS (Sistema de prevenció d'intrusos). Capaç de realitzar anàlisis de tràfic en temps real.

Snort té quatre modes:
1. Sniffer: Printa per pantalla els paquets que rep, com Wireshark.
2. Logger: Guarda els paquets rebuts al disc.			
3. IDS: Detecta intrusions en la xarxa i possibles atacs.
4. IPS: Detecta intrusions en la xarxa i possibles atacs, i en pot denegar l'acces.

En aquest últim mode alguns dels tipus d'atac que Snort pot detectar són:		
- El desbordament de memòria intermèdia(buffer).		
- Escaneig de ports		
- Atac de denegació de servei (DoS)		
- Atac a executables CGI
