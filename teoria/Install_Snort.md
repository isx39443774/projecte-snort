# Instal·lació i configuració de Snort

1. Per instalar Snort haurem de dirigir-nos a un terminal i realitzar la següent comanda:
```
yum install https://www.snort.org/downloads/snort/snort-2.9.16-1.f31.x86_64.rpm
```

2. Un cop instal·lat, editarem el fitxer **/etc/snort/snort.conf** el qual es el fitxer de configuració. Haurem de modificar la variable **HOME_NET** i canviar-la per la nostre ip local, o ip+mx de la  xarxa a protegir.
![](../aux/home_net.png)

3. Com que en el nostre cas volem crear noves regles, comentarem totes les linies de les regles.
![](../aux/comentar_rules.png)

4. Seguidament per comprovar que el nostre fixer de configuració es correcte realizarem la següent instrucció:
```
snort -T -c /etc/snort/snort.conf
```

5. Ja tenim el fitxer de configuració comprovat, així que ara ens toca iniciar Snort amb la següent instrucció:
```
snort -c /etc/snort/snort.conf -i enp2s0
```
Aquesta instrucció no ens printara les alertes per pantalla, si volem veure les alertes per pantalla hem d'afegir el ```-A console```.
Després d'una gran parrafada de informació ens hauria de quedar així:
![](../aux/comprovacio.png)

Si cada cop que iniciem Snort no volem que ens printi aquesta parrafada, amb el **-q** executarem snort en mode quiet.

## Possible error
Un possible error que ens podem trobar es que Snort no trobi les llibreries compartides libdnet.1
![](../aux/lib_error.png)

Per solventar l'error, creearem un enllaç simbolic:    
![](../aux/lib_solved.png)
