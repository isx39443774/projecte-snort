# Snort

## Què és Snort

![](../aux/snort_img.png)

## Com funciona Snort

![](../aux/snort_esquema.png)

## Modes de Snort
- Sniffer

- Logger

- IDS

- IPS

## Mode IDS/IPS

![](../aux/snort_alert_drop.png)


## Parametres Snort

- -A

- -c

- -D

- -K

- -i

- -h

- -Q

- -l

## Regles de Snort

Sintaxis mínim de les regles:

- Acció

- Protocol

- IP origen

- Port origen

- IP destí

- Port destí

- msg

- sid


## Exemple Alerta

![](../aux/exemple_regla.png)

![](../aux/exemple_regla_alert.png)

## Instal·lar Snort

![](../aux/home_net.png)

![](../aux/comentar_rules.png)
